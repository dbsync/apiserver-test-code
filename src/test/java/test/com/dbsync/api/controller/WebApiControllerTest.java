/**
 * 
 */
package test.com.dbsync.api.controller;

import java.lang.reflect.Method;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import com.appmashups.appcode.AppCodeConstants;
import com.dbsync.api.container.AppCodeStorage;

import com.google.gson.Gson;

/**
 * @author user
 *
 */
public class WebApiControllerTest {

	
	private final static String APPCODE_CONTAINER = "sandbox";
	private final static String APPCODE_USER = "appCodeName";
	
	private final static Properties props = new Properties();

	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	private Properties appCodeStorageProperties;
	
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		props.put("config.folder", "/Users/rajeev/Documents/rajeev/avankia/dbsync-api-management/src/main/webapp/WEB-INF/");
		request = new MockHttpServletRequest("GET", "/OdataService.svc/HelloParameters");
		request.setContextPath("/webapi");
		request.setServletPath("/OdataService.svc");
		request.addParameter(APPCODE_CONTAINER, "sandbox");
		request.addParameter(APPCODE_USER, "HelloWorldCode");
		request.setCharacterEncoding("UTF-8");
		response = new MockHttpServletResponse();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		Properties props = new Properties();
		props.put("config.folder", "/Users/rajeev/Documents/rajeev/avankia/dbsync-api-management/src/main/webapp/WEB-INF/");

		AppCodeStorage container = new AppCodeStorage(props);

		// get env and app code name from request
		//Object appCode = container.findAppCode("sandbox", "ReplicationAPIDriver");
		Object appCode=null;
		try {
			appCode = container.findAppCode("sandbox", "ReplicationAPIDriver");//"dbsync-repl-engine-3.0");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Method function = container.findFunction(appCode, "replicate");
		System.out.println(function.getName());
		
		Class<?> incomingParameterClass = container.findAppCodeParameter(function, AppCodeConstants.IN);
		Class<?> outgoingParameterClass = container.findAppCodeParameter(function, AppCodeConstants.OUT);
		
		
		try {
			Object incomingParamInstance = incomingParameterClass.getConstructors()[0].newInstance();
			Gson gson = new Gson();
			//Object to JSON in String
			String jsonInString = gson.toJson(incomingParamInstance);
			System.out.println(jsonInString);
			
			Object result = function.invoke(appCode, incomingParamInstance);
			System.out.println(gson.fromJson(jsonInString, result.getClass()));
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		
		System.out.println(incomingParameterClass.getName());
		System.out.println(outgoingParameterClass.getName());
	}

}
