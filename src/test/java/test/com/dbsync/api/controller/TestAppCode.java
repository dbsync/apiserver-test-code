package test.com.dbsync.api.controller;

import java.util.Date;
import java.util.List;

import com.appmashups.appcode.AppCodeConstants;
import com.appmashups.appcode.annotations.AppCodeApiName;
import com.appmashups.appcode.annotations.AppCodeFunction;
import com.appmashups.appcode.annotations.AppCodeParameter;

@AppCodeApiName(name="ReplicationAPIDriver")
public class TestAppCode {

	/**
	 * Wrapper Class to hold all config properties to initiate replication
	 */
	@AppCodeParameter(name="ReplicationParam", type=AppCodeConstants.IN)
	public static class ReplicationParams {
		
		public boolean emailError=false;
		public String configDir;
		public String callbackURL;
		public String soapEndpoint;
	}
	
	/**
	 * Used to return status when 
	 * 1. Replication is called.
	 * 2. GetStatus API is called.
	 */
	@AppCodeParameter(name="Status", type = AppCodeConstants.OUT)
	public class Status {
		
		public String status;
		public Date submitTime;
		public String processId;
		public String callbackURL;
		public ReplicationParams param;
	}
	
	@AppCodeFunction(name="replicate", type = AppCodeConstants.READ, parameterName="ReplicationParam")
	public List<Status> replicate(ReplicationParams params) {
		return null;
	}
}
