package test.com.dbsync.api.appcode;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;

import com.dbsync.api.container.AppCodeStorage;

public class AppCodeAnnotationTest {

	private final static Log LOGGER = LogFactory.getLog(AppCodeAnnotationTest.class);

	@Test
	public void testAnnotatedCode() throws RemoteException, MalformedURLException {
		Properties props = new Properties();
		props.put("config.folder", "C:/workspace/salesforce/dbsync-api-management/src/main/webapp/WEB-INF/");

		AppCodeStorage container = new AppCodeStorage(props);

		Object appCode = container.findAppCode("sandbox", "myappcode.HelloWorldSvc");
		Method function = container.findFunction(appCode, "readHello");
		System.out.println(function.getName());
		
		appCode = container.findAppCode("sandbox", "helloWorld");
		function = container.findFunction(appCode, "readData");
		System.out.println(function.getName());
	}
}
