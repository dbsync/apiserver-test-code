package test.com.dbsync.api.odata.testdata;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appmashups.appcode.AppCode;
import com.appmashups.appcode.AppCodeConstants;
import com.appmashups.appcode.Context;
import com.appmashups.appcode.SaveResult;
import com.appmashups.appcode.annotations.AppCodeFunction;
import com.appmashups.appcode.annotations.AppCodeParameter;

public class HelloWorldSvc implements AppCode {
		
		
		@AppCodeParameter(name = "HelloParameter", type = AppCodeConstants.OUT)
		public static class HelloParameter{
			public String id;
			public String name;
			public String value;
		}
		
		@AppCodeParameter(name = "HelloFilter", type = AppCodeConstants.IN)
		public static class HelloFilter{
			public String key;
		}
		
		private List<HelloParameter> initParams ;
		
		private String sessionKey;
		
		static Logger logger = LoggerFactory.getLogger(HelloWorldSvc.class);

		public void close() throws RemoteException {
			logger.info("Hello World AppCode close called:" + sessionKey);
			System.out.println("Hello World AppCode close called:" + sessionKey );
			sessionKey = null;

		}

		public void open() throws RemoteException {
			init();
			sessionKey = UUID.randomUUID().toString();
			logger.info("Hello World AppCode Session opened:" + sessionKey);
			System.out.println("Hello World AppCode Session opened:" + sessionKey);

		}

		public void setContext(Context arg0) {
			// TODO Auto-generated method stub
			logger.info("Hello World AppCode Set Context called");
			System.out.println("Hello World AppCode Set Context called");

		}
		
		private void init() {
			
			initParams = new ArrayList<HelloWorldSvc.HelloParameter>();
			
			HelloParameter initparam1 = new HelloParameter();
			initparam1.id = "1";
			initparam1.name = "Scott";
			
			HelloParameter initparam2 = new HelloParameter();
			initparam2.id = "2";
			initparam2.name = "Tiger";
			
			HelloParameter initparam3 = new HelloParameter();
			initparam3.id = "3";
			initparam3.name = "Matt";
			
			
			initParams.add(initparam1);
			initParams.add(initparam2);
			initParams.add(initparam3);
			
			
		}
		
		
		@AppCodeFunction(name = "HelloParameters", type = AppCodeConstants.READ, parameterName = "HelloParameter")
		public List<HelloParameter> readHello(HelloFilter filter){
			
			logger.info("Received Read operation with filter:" + filter.key + ":with session" + sessionKey);
			
			System.out.println("Received Read operation with filter:" + filter.key + ":with session" + sessionKey);

			
			List<HelloParameter> returnList = new ArrayList<HelloWorldSvc.HelloParameter>();
			
			for(HelloParameter initParam : initParams) {
				
				if(filter.key.equalsIgnoreCase(initParam.name)) {
					
					//Now set the value to "Hello":name:sessionId
					initParam.value = "Hello:"+initParam.name+":"+sessionKey;

					System.out.println("Matched Key:" + initParam.name + ":value:" + initParam.value );
					
					logger.info("Matched Key:" + initParam.name + ":value:" + initParam.value );
					
					
					returnList.add(initParam);
				}
			}
			
			return returnList;
		}
		
		
		@AppCodeFunction(name = "HelloParameters", type = AppCodeConstants.WRITE, parameterName = "HelloParameter")
		public SaveResult[] writeHello(List<HelloParameter> inputParams){
			
			List<SaveResult> saveResults = new ArrayList<SaveResult>();
			
			logger.info("Received write operation with session" + sessionKey);
			
			System.out.println("Received write operation with session" + sessionKey);
			
			for(HelloParameter inputParam : inputParams) {
				
				System.out.println("Received input parameter:id:" + inputParam.id + ":name:" + inputParam.name);
				logger.info("Received input parameter:id:" + inputParam.id + ":name:" + inputParam.name);
				
				inputParam.value = "Hello:"+inputParam.name+":"+sessionKey;
				System.out.println("Writing value:" + inputParam.value );
				logger.info("Writing value:" + inputParam.value);
				
				SaveResult result = new SaveResult();
				result.id = inputParam.id;
				result.newid = inputParam.id+":"+sessionKey;
				result.success = true;
				
				saveResults.add(result);
				
			}
			
			return saveResults.toArray(new SaveResult[] {});
		}

	}