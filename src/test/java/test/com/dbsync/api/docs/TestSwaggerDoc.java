package test.com.dbsync.api.docs;

import org.junit.Before;
import org.junit.Test;

import test.com.dbsync.api.controller.TestAppCode;

import com.dbsync.api.container.SwaggerBuilder;

public class TestSwaggerDoc {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		SwaggerBuilder sb = new SwaggerBuilder();
		String json = sb.toJSON(TestAppCode.class);
		System.out.println(json);
		
	}

}
