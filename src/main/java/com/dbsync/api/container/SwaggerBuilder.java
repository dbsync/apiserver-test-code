package com.dbsync.api.container;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import com.appmashups.appcode.annotations.AppCodeFunction;
import com.appmashups.appcode.annotations.RequestMethod;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class SwaggerBuilder {
	class Info {
		String title;
		String description;
		String termsOfService;
		License license;
		String version;
	}

	class License {
		String name = "DBSync API License";
		String url = "http://www.mydbsync.com/eula";
	}

	Gson gson;
	String hostPort = "localhost:8080";
	String basePath = "/webapi";
	JsonArray consumeOrProduce;
	Map<String, Class<?>> entityTypeMap = new HashMap<>();

	public SwaggerBuilder() {
		gson = new GsonBuilder().setPrettyPrinting().create();
		consumeOrProduce = toJsonArray(new String[] { "application/json" });
	}

	public SwaggerBuilder(String hostPort, String context, String environment, String plugin) {
		gson = new GsonBuilder().setPrettyPrinting().create();
		this.hostPort = hostPort;
		basePath = context + "/appcode/" + environment + "/" + plugin;
		consumeOrProduce = toJsonArray(new String[] { "application/json" });
	}

	public String toJSON(Class<?> cls) {
		return gson.toJson(toSwagger(cls));
	}

	private JsonObject toSwagger(Class<?> cls) {
		JsonObject json = new JsonObject();

		json.addProperty("swagger", "2.0");

		Info info = new Info();
		info.version = "1.0";
		info.title = cls.getSimpleName();
		info.license = new License();

		json.add("info", gson.toJsonTree(info));
		json.addProperty("host", hostPort);
		json.addProperty("basePath", basePath);
		if (hostPort.startsWith("localhost")) {
			json.add("schemes", toJsonArray(new String[] { "http" }));
		}
		json.add("consumes", consumeOrProduce);
		json.add("produces", consumeOrProduce);

		Class<?>[] exposedClasses = cls.getDeclaredClasses();
		for (Class<?> expcls : exposedClasses) {
			String key = expcls.getSimpleName();
			entityTypeMap.put(key, expcls);
		}

		createPaths(cls, json);

		createDefinitions(cls, json);

		return json;
	}

	private JsonArray toJsonArray(String[] arr) {
		JsonArray jar = new JsonArray();
		for (String s : arr) {
			jar.add(s);
		}
		return jar;
	}

	private void createPaths(Class<?> cls, JsonObject json) {
		Method[] allFunctions = cls.getMethods();
		List<Method> functionList = new ArrayList<Method>();

		for (Method func : allFunctions) {
			if (func.getAnnotation(AppCodeFunction.class) != null) {
				// look at in and out params and create the specs
				Class<?>[] argTypes = func.getParameterTypes();
				for (Class<?> args : argTypes) {

					if (!entityTypeMap.containsKey(args.getName())) {
						entityTypeMap.put(args.getName(), args);
					}
				}
				Class<?> retType = func.getReturnType();
				Type retGenericType = func.getGenericReturnType();
				String typeName = retGenericType.getTypeName();

				if (retGenericType instanceof ParameterizedType) {
					typeName = ((ParameterizedType) retGenericType).getActualTypeArguments()[0].getTypeName();
					retType = entityTypeMap.get(typeName);
					if (retType == null) {
						try {
							if (typeName.startsWith("java")) {
								retType = Class.forName(typeName);
							} else {
								retType = Class.forName(typeName, false, cls.getClassLoader());
							}
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						}
					}
				}

				if (!entityTypeMap.containsKey(typeName)) {
					try {
						entityTypeMap.put(retType.getName(), Class.forName(typeName, false, retType.getClassLoader()));
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				}

				functionList.add(func);
			}
		}

		JsonObject paths = new JsonObject();

		for (Method func : functionList) {
			JsonObject methods = new JsonObject();
			for (RequestMethod reqMethod : func.getAnnotation(AppCodeFunction.class).method()) {
				methods.add(reqMethod.name().toLowerCase(), toFunction(func, reqMethod));
			}
			paths.add("/" + func.getAnnotation(AppCodeFunction.class).name(), methods);

		}

		json.add("paths", paths);

	}

	private JsonObject toFunction(Method func, RequestMethod reqMethod) {
		JsonObject postItem = new JsonObject();
		postItem.addProperty("description", "");
		postItem.add("produces", consumeOrProduce);

		JsonArray params = new JsonArray();

		if (func.getParameterCount() > 0) {
			JsonObject param = new JsonObject();
			Class<?> paramClass = func.getParameterTypes()[0];
			String ref = paramClass.getSimpleName();

			if (RequestMethod.GET.name().equalsIgnoreCase(reqMethod.name())) {
				Stream.of(paramClass.getFields()).forEach(f -> {
					param.addProperty("name", f.getName());
					param.addProperty("in", "query");
					param.addProperty("required", false);
				});
			}
			if (RequestMethod.POST.name().equalsIgnoreCase(reqMethod.name()) 
					|| RequestMethod.PUT.name().equalsIgnoreCase(reqMethod.name())
					|| RequestMethod.DELETE.name().equalsIgnoreCase(reqMethod.name())) {
				param.addProperty("name", "body");
				param.addProperty("in", "body");
				param.addProperty("required", true);
			}
			JsonObject jsonRef = new JsonObject();
			jsonRef.addProperty("$ref", "#/definitions/" + ref);
			param.add("schema", jsonRef);
			params.add(param);
		}

		postItem.add("parameters", params);

		// responses
		JsonObject resp = new JsonObject();
		// 200 and Error
		resp.add("200", toResponse(func));

		postItem.add("responses", resp);

		return postItem;
	}

	private JsonObject toResponse(Method func) {

		Class<?> retType = func.getReturnType();
		Type retGenericType = func.getGenericReturnType();
		String typeName = retGenericType.getTypeName();
		boolean isList = false;
		if (retGenericType instanceof ParameterizedType) {
			typeName = ((ParameterizedType) retGenericType).getActualTypeArguments()[0].getTypeName();

			isList = true;
		}

		JsonObject ret = new JsonObject();
		ret.addProperty("description", "todo");

		JsonObject schema = new JsonObject();

		retType = entityTypeMap.get(typeName);

		if (retType.isArray() || isList) {
			schema.addProperty("type", "array");
		} else {
			schema.addProperty("type", "object");
		}
		if (typeName.lastIndexOf("$") > 0) {
			typeName = typeName.substring(typeName.lastIndexOf("$") + 1);
		} else if (typeName.lastIndexOf(".") > 0) {
			typeName = typeName.substring(typeName.lastIndexOf(".") + 1);
		}

		JsonObject items = new JsonObject();
		items.addProperty("$ref", "#/definitions/" + typeName);
		schema.add("items", items);

		ret.add("schema", schema);
		return ret;
	}

	private void createDefinitions(Class<?> cls, JsonObject json) {

		JsonObject definitions = new JsonObject();

		Class<?>[] exposedClasses = cls.getDeclaredClasses();
		for (Class<?> expcls : exposedClasses) {

			String key = expcls.getSimpleName();

			entityTypeMap.put(key, expcls);

			definitions.add(key, toDefinition(expcls));
		}
		json.add("definitions", definitions);

	}

	private JsonElement toDefinition(Class<?> expcls) {
		JsonObject el = new JsonObject();
		el.addProperty("type", "object");

		JsonObject properties = new JsonObject();

		Field[] fields = expcls.getFields();

		for (Field fl : fields) {
			JsonObject prop = toProperties(fl);
			if (prop != null) {
				properties.add(fl.getName(), prop);
			}
		}

		el.add("properties", properties);

		return el;
	}

	private JsonObject toProperties(Field fl) {
		JsonObject flprop = new JsonObject();

		if (fl.getType().isArray()) {
			flprop.addProperty("$ref", "#/definitions/" + fl.getType().getComponentType().getSimpleName());
		} else if (fl.getType().isPrimitive() || fl.getType().getPackage().getName().startsWith("java")) {
			flprop.addProperty("type", fl.getType().getSimpleName().toLowerCase());
		} else {
			if (entityTypeMap.containsKey(fl.getType().getSimpleName())) {
				flprop.addProperty("$ref", "#/definitions/" + fl.getType().getSimpleName());
			} else {
				flprop.addProperty("type", "ERROR:" + fl.getType().getName());
				return null;
			}

		}
		return flprop;
	}

}
