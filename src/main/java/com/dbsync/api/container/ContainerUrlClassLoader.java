package com.dbsync.api.container;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.logging.Logger;

class ContainerUrlClassLoader extends URLClassLoader {

	private static Logger logger = Logger.getLogger(ContainerUrlClassLoader.class.getName());

	URL[] referedURL;
	ClassLoader myParent;

	public ContainerUrlClassLoader(URL[] urls, ClassLoader parent) {
		super(urls, null);
		this.referedURL = urls;
		this.myParent = parent;
	}

	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		try {
			logger.fine("PLugin looking up:" + name + " found :" + (super.findClass(name) != null));
		} catch (Throwable e) {
			logger.fine("PLugin looking up:" + name + " not found -" + e.getMessage());
		}
		return super.findClass(name);
	}

	public Class loadClass(String name) throws ClassNotFoundException {
		logger.fine("Plugin loading class:" + name);
		Class c = findLoadedClass(name);
		if (c == null) {
			try {
				// c = getParent().loadClass(name);
				if (myParent != null)
					c = myParent.loadClass(name);
			} catch (ClassNotFoundException e) {
			}
			if (c == null)
				try {
					c = findClass(name);
				} catch (Throwable e) {
					logger.severe("CLASS ERROR:" + name + "\n" + e.getMessage());
					// e.printStackTrace();
				}
		}
		return c;
	}

}