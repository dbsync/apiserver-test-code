package com.dbsync.api.container;

import java.io.File;
import java.io.FilenameFilter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.dbsync.api.container.AppCodeStorage.AppCodeListenerEvent;

public class AppCodeContainer {
	static Logger log = Logger.getLogger(AppCodeContainer.class.getName());

	// @Autowired
	@SuppressWarnings("serial")
	public static Map<String, AppCodeStorage> containerMap = new HashMap<String, AppCodeStorage>(){
		protected void finalize() throws Throwable{
			System.out.println("CONTAINER MAP FINALIZE");
		}
	};
	
	@Deprecated
	public static AppCodeStorage getStorage(String key){
		AppCodeStorage storage = AppCodeContainer.containerMap.get(key);
		if (storage==null){
			storage = new AppCodeStorage();
			containerMap.put(key, storage);
		}
		return storage;
	}
	
	public static AppCodeStorage getStorage(String env, String appCode, String runDir){
		String key = env +"/"+ appCode;
		log.info("Loading AppCode:"+ key);
		
		AppCodeStorage storage = AppCodeContainer.containerMap.get(key);
		if (storage==null){
			storage = new AppCodeStorage();
			storage.setRunDir(runDir);
			
			storage.findAppCode(env, appCode);
			containerMap.put(key, storage);
		}
		return storage;
	}
	

	
	
	public static Map<String, AppCodeStorage> getAllStorage(String runDir){
		
		new AppCodeStorage().loadAll(new AppCodeListenerEvent (){

			@Override
			public void loadEvent(String env, String appCodeName) {
				
				try {
					getStorage(env, appCodeName, runDir);
				} catch (Exception e){
					log.severe("Error in loading:"+env+"/"+appCodeName);
				}
				
			}
			
		});
		
		return containerMap;
	}
	
	public static void removeStorage(String env, String appCode){
		String key = env +"/"+ appCode;
		log.info("Removing cached Storage for AppCode:"+ key);
		
		AppCodeContainer.containerMap.remove(key);
		
	}
}
