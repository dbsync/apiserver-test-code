package com.dbsync.api.container;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.springframework.stereotype.Component;

import com.appmashups.appcode.AppCode;
import com.appmashups.appcode.AppCodeConstants;
import com.appmashups.appcode.Context;
import com.appmashups.appcode.annotations.AppCodeApiName;
import com.appmashups.appcode.annotations.AppCodeFunction;
import com.appmashups.appcode.annotations.AppCodeParameter;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

@SuppressWarnings({ "unchecked" })

@Component
public class AppCodeStorage {

	Map<String, Class<AppCode>> cache = new HashMap<String, Class<AppCode>>();
	
	Map<String, File> appCodeToDefaultFolderMap = new HashMap<String, File>();

	Map<String, File> appCodeToRootFolderMap = new HashMap<String, File>();

	Map<String, Class<?>> functionIncomingParametersCache = new HashMap<String, Class<?>>();

	Map<String, Class<?>> functionOutgoingParametersCache = new HashMap<String, Class<?>>();

	private static final String LEGACY_CODE_NAME_PREFIX = "EMPTY_";

	private static final String CODE_CONTAINER_DELIMITER = "|";

	protected ClassLoader classLoader;

	final static String FILE_URL_PREFIX = "file://";

	private final static String appCodeStoragePath = "/conf/db/%s/_appcode/";

	private final static String username = "localhost@avankia.com";

	private Properties props;
	
	private String runDir;

	// Modified by sambit to make the user know that classname is for internal
	// use
	// final String CLASS_NAME = "classname";

	public static Logger logger = Logger.getLogger(AppCodeStorage.class.getName());

	
	public static class AppCodeConfigJSON{
		List<AppCodeJSON> appCode = new ArrayList<AppCodeJSON>();
		List<ConfigJSON> config = new ArrayList<ConfigJSON>();
		
	}
	public static class AppCodeJSON{
		String className;
		String location;
	}
	public static class ConfigJSON{
		String key;
		String value;
	}
	
	public AppCodeStorage(Properties props) {
		this.props = props;
	}

	public void setRunDir(String runDir) {
		this.runDir = runDir;
		
	}
	
	public Object getAppCodeFromCache(String key) {
		
		try {
			Class cachedClass = cache.get(key);
			if (cachedClass!=null){
				return cache.get(key).newInstance();
			} else {
				return null;
			}
			
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			return null;
		}
	}

	
	
	//
	
	interface AppCodeListenerEvent {
		public void loadEvent(String environment, String appCodeName);
	}
	
	public void loadAll(AppCodeListenerEvent listener){
		Map<String, AppCodeStorage> allAppCode = new HashMap<String, AppCodeStorage>();
		
		final String relativePathToAppCode = String.format(appCodeStoragePath, username);
		final String storageRootPath = props != null ? props.getProperty("config.folder") : new File(getClass().getResource("/").getFile()).getParent();
		
		// check each directory in storage check the deployed apicode
		
		File appCodeStorageDir = new File(storageRootPath + relativePathToAppCode);
		String [] allEnv = appCodeStorageDir.list((dir, name) -> (!name.startsWith(".")));
		
		for (String environment:  allEnv){
			logger.fine("dir:"+ environment);
			
			File appCodeStorage = new File(storageRootPath + relativePathToAppCode + environment);
			if(appCodeStorage.isDirectory()){
				File[] appCodeFolders = appCodeStorage.listFiles((dir, name) -> (!name.startsWith(".")));
				
				for (int i = 0; i < appCodeFolders.length; i++) {
					String appcodeName = appCodeFolders[i].getName();
					
					if (listener != null){
						listener.loadEvent(environment, appcodeName);
					}
					
				}
			}
		}
		
	
		
	}
	
	//
	public Object findAppCode(String environment, String appcodeName) {

		Class<AppCode> appCodeClassCached = cache.get(appcodeName);
		if (appCodeClassCached!=null){
			try {
				return appCodeClassCached.newInstance();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
		
		AppCode toReturn = null;
		final String relativePathToAppCode = String.format(appCodeStoragePath, username);
		final String storageRootPath = props != null ? props.getProperty("config.folder") : new File(getClass().getResource("/").getFile()).getParent();
		final File appCodeStorage = new File(storageRootPath + relativePathToAppCode + environment);
		if (!appCodeStorage.exists()) {
			return toReturn;
		}

		File[] appCodeFolders = appCodeStorage.listFiles(new FilenameFilter(){

			@Override
			public boolean accept(File dir, String name) {
				return (!name.startsWith("."));
			}
			
		});

		for (int i = 0; i < appCodeFolders.length; i++) {

			if (!appCodeFolders[i].getName().equals(appcodeName)) {
				continue;
			}

			logger.info("loading appcode folder :"+appCodeFolders[i].getName());

			/*
			 * now get all the published appcode from config/appcode.xml file.
			 * the file is as
			 * <AppCodeService>
			 * 	<AppCode>
			 * 		<class>my.appcode.MyClass</class>
			 * 	</AppCode>
			 * 	<AppCode>
			 * 		<class>my.appcode.MyClass</class>
			 * 	</AppCode>
			 * </AppCodeService>
			 * 
			 */
			File appCodeDef = new File(appCodeFolders[i]+"/conf/appcode.appj");
			if (!appCodeDef.exists()){
				continue;
			}
			Gson gson = new Gson();
			Map<String, String> appCodeEntries = new HashMap<String, String>();
			try {
				
				AppCodeConfigJSON appConfig = gson.fromJson(new FileReader(appCodeDef), AppCodeConfigJSON.class);
				
				final Map<String, String> retval = new HashMap<String, String>();
				
				for (AppCodeJSON ac: appConfig.appCode){
					
					ArrayList<URL> clsUrlArray = new ArrayList<URL>();
					
					File rootFolder = appCodeFolders[i];
					
					// if code location override, then use the one from location, else look from directory
					if (ac.location !=null){
						if (ac.location.indexOf("@RUNDIR")>=0){
							ac.location = ac.location.replace("@RUNDIR", runDir);
						}
						rootFolder = new File(ac.location);
						try {
							clsUrlArray.add(new File(rootFolder,"classes").toURI().toURL());
							clsUrlArray.add(new File(rootFolder,"bin").toURI().toURL());
						} catch (MalformedURLException e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}
						// get all in the libs
						rootFolder = new File(ac.location,"lib");
					} 
					
					{	
						URL[] clsURL = null;

						String[] files = rootFolder.list();

						try {
							clsUrlArray.add(new File(rootFolder,"bin").toURI().toURL());
						} catch (MalformedURLException e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}

						
						for (String file : files) {
							File f = new File(rootFolder, file);
							if (f.getName().endsWith(".jar")) {
								try {
									String jarURL = "jar:" + f.toURL() + "!/";
									clsUrlArray.add(new URL(jarURL));
								} catch (MalformedURLException muex) {
									logger.fine("Failed to load jars");
								} catch (Exception e) {
									logger.fine("Failed to load jars");
								}
							}
						}

						clsURL = clsUrlArray.toArray(new URL[clsUrlArray.size()]);
						
						logger.info("Classpath:");
						
						for (URL u: clsURL)
							logger.info(u.toString());

						classLoader = new URLClassLoader(clsURL, AppCodeStorage.class.getClassLoader());
						
						Class<?> cls=null;
						try {
							cls = this.classLoader.loadClass(ac.className);
							String className = cls.getName();
							if (isAppCodeClass(cls)) {
								AppCodeApiName webApiAnnotation = cls.getAnnotation(AppCodeApiName.class);
								if (webApiAnnotation != null) {
									retval.put(webApiAnnotation.name(), className);
									
									appCodeToDefaultFolderMap.put(webApiAnnotation.name(),appCodeFolders[i]);
									appCodeToRootFolderMap.put(webApiAnnotation.name(),rootFolder);
								//} else {
								//	retval.put(cls.getPackage().getName() + CODE_CONTAINER_DELIMITER + LEGACY_CODE_NAME_PREFIX + className, className);
								}
							}
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							logger.severe(e.getMessage());
						}
					}
					
					appCodeEntries.putAll(retval);
				}
				logger.info("AppCode config/appcode.appj loaded");
			} catch (JsonSyntaxException e1) {
				//throw new Exception("In correct JSON syntax in conf/appcode.appj");
			} catch (JsonIOException e1) {
				//throw new Exception("Unable to parse JSON from conf/appcode.appj");
			} catch (FileNotFoundException e1) {
				//throw new Exception("File not present - conf/appcode.appj");
			}
			
			
			

			try {

				/*
				for (String file : files) {
					File f = new File(appCodeFolders[i], file);
					if (f.getName().endsWith(".jar")) {
						try {
							appCodeEntries.putAll(getResourcesFromJarFile(f));
						} catch (MalformedURLException muex) {
							logger.fine("Failed to load jars");
						} catch (Exception e) {
							logger.fine("Failed to load appCode classes from jars");
						}
					}
				}
				*/
				if (appCodeEntries.isEmpty()) {
					continue;
				}
				Class<AppCode> appCodeClass = null;
				final Set<String> appCodeKeys = appCodeEntries.keySet();
				String selectedCodeClass = "";
				for (String appCodeKey : appCodeKeys) {
					int appCodeNameStartPosition = appCodeKey.indexOf(LEGACY_CODE_NAME_PREFIX) != -1 ? appCodeKey.indexOf(LEGACY_CODE_NAME_PREFIX) + 1
							: appCodeKey.indexOf(CODE_CONTAINER_DELIMITER) + 1;
					final String appCodeNameKey = appCodeKey.substring(appCodeNameStartPosition);
					if (appCodeNameKey.equals(appcodeName)) {
						selectedCodeClass = appCodeEntries.get(appCodeKey);
						break;
					}
				}
				if (selectedCodeClass != null && !selectedCodeClass.isEmpty()) {
					appCodeClass = (Class<AppCode>) ((URLClassLoader) classLoader).loadClass(selectedCodeClass);
				}
				if (appCodeClass == null) {
					continue;
				}
				Constructor<AppCode>[] constructors = (Constructor<AppCode>[]) appCodeClass.getConstructors();
				if (constructors == null || constructors.length == 0) {
					throw new ClassNotFoundException("App code constructor not found!");
				}
				try {
					toReturn = appCodeClass.newInstance();//constructors[0].newInstance();
					//cache.put(appcodeName, toReturn);
					cache.put(appcodeName, appCodeClass);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

			return toReturn;
		}
		return null;
	}

	/**
	 * @param file
	 * @return Map with appcode classes as key and appCodeName as value
	 * @throws Exception
	 * @deprecated
	 */
	private Map<String, String> getResourcesFromJarFile(final File file) throws Exception {
		final Map<String, String> retval = new HashMap<String, String>();
		ZipFile zf;
		try {
			zf = new ZipFile(file);
		} catch (final ZipException e) {
			throw new Error(e);
		} catch (final IOException e) {
			throw new Error(e);
		}
		final Enumeration<? extends ZipEntry> e = zf.entries();
		while (e.hasMoreElements()) {
			final ZipEntry ze = (ZipEntry) e.nextElement();
			final String fileName = ze.getName();
			final boolean accept = fileName.endsWith(".class");
			if (accept) {
				String className = fileName.replace("/", ".").replace(".class", "");
				Class<?> cls = buildClass(className);

				if (isAppCodeClass(cls)) {
					AppCodeApiName webApiAnnotation = cls.getAnnotation(AppCodeApiName.class);
					if (webApiAnnotation != null) {
						retval.put(file.getName() + CODE_CONTAINER_DELIMITER + webApiAnnotation.name(), className);
					} else {
						retval.put(file.getName() + CODE_CONTAINER_DELIMITER + LEGACY_CODE_NAME_PREFIX + className, className);
					}
				}
			}
		}
		try {
			zf.close();
		} catch (final IOException e1) {
			throw new Error(e1);
		}
		return retval;
	}

	private boolean isAppCodeClass(Class<?> cls) {

		if (cls == null) {
			return false;
		}
		try {
			if (cls.getInterfaces() != null && cls.getInterfaces().length > 0) {
				boolean hasAppCodeInterface = false;

				for (Class<?> intClass : cls.getInterfaces()) {
					try {
						hasAppCodeInterface = intClass.getName().toLowerCase().contains("appcode");
						if (hasAppCodeInterface) {
							break;
						}
					} catch (Exception ex) {
						ex.printStackTrace();
					} catch (Error err) {
						err.printStackTrace();
					}
				}

				if (hasAppCodeInterface) {
					return true;
				} else {
					return false;
				}
			}

			return false;
		} catch (Exception ex) {
			logger.warning("Error on parse interfaces!");
			ex.printStackTrace();
		}
		return false;
	}

	private Class<?> buildClass(String className) throws Exception {

		Class<?> cls = null;
		try {
			try {
				cls = classLoader.loadClass(className);
			} catch (Exception e) {
				logger.severe("Class not found:" + e.getMessage());
				return cls;
			}
		} catch (Exception ex) {
			logger.log(Level.WARNING, "class loading failed", ex);
		} catch (Error err) {
			logger.log(Level.WARNING, className + " class loading failed");
		}

		return cls;
	}

	public AppCodeStorage() {
		
	}

	public Context getContext() {
		return Context.buildContext(this.props);
	}

	public Method findFunction(Object appCode, String functionName) {
		Class<?> appCodeClass = appCode.getClass();
		Method[] appCodeMethods = appCodeClass.getMethods();
		Method functionToReturn = null;
		for (Method appCodeMethod : appCodeMethods) {
			Annotation[] methodAnnotations = appCodeMethod.getAnnotations();
			for (Annotation annotation : methodAnnotations) {
				if (annotation != null && annotation instanceof AppCodeFunction) {
					AppCodeFunction functionAnnotation = (AppCodeFunction) annotation;
					if (functionName.equals(functionAnnotation.name())) {
						functionToReturn = appCodeMethod;
						break;
					}
				}
			}
		}
		return functionToReturn;
	}

	public Class<?> findAppCodeParameter(Method appCodeFunction, String parameterType) {
		Class<?>[] declaredClasses = appCodeFunction.getDeclaringClass().getDeclaredClasses();
		AppCodeFunction appCodeFunctionAnnotation = appCodeFunction.getAnnotation(AppCodeFunction.class);
		if (appCodeFunctionAnnotation == null) {
			return null;
		}
		final String parameterName = appCodeFunctionAnnotation.parameterName();
		if (declaredClasses == null) {
			return null;
		}

		if (parameterType.equals(AppCodeConstants.OUT)) {
			Type listType = appCodeFunction.getGenericReturnType();
			if (listType instanceof ParameterizedType) {
				final String returnTypeName = ((ParameterizedType) listType).getActualTypeArguments()[0].getTypeName();
				for (Class<?> declaredClass : declaredClasses) {
					if (returnTypeName.equals(declaredClass.getName())) {
						return declaredClass;
					}
				}
			}

			return appCodeFunction.getReturnType();
		}

		for (Class<?> declaredClass : declaredClasses) {
			Annotation[] parameterAnnotations = declaredClass.getAnnotations();
			if (parameterAnnotations != null) {
				for (Annotation annotation : parameterAnnotations) {
					if (annotation instanceof AppCodeParameter) {
						if (parameterName.equals(((AppCodeParameter) annotation).name()) && parameterType.equals(AppCodeConstants.OUT)) {
							return declaredClass;
						}
					}
				}
			}
		}

		for (Class<?> declaredClass : declaredClasses) {
			Annotation[] parameterAnnotations = declaredClass.getAnnotations();
			if (parameterAnnotations != null) {
				for (Annotation annotation : parameterAnnotations) {
					if (annotation instanceof AppCodeParameter) {
						if (parameterName.equals(((AppCodeParameter) annotation).name()) && parameterType.equals(AppCodeConstants.IN)) {
							return declaredClass;
						}
					}
				}
			}
		}
		return null;
	}

	public void reloadAppCode(String appCodePath) {
		// TODO Auto-generated method stub

	}

	public byte[] findResource(String environment, String appcodeName,
			String resource) {
		

		File actualAppCodeDefaultDir = this.appCodeToDefaultFolderMap.get(appcodeName);
		final File resourceFile = new File(actualAppCodeDefaultDir,resource);
		
		InputStream in=null;
		try {
			in = new FileInputStream(resourceFile);
			if (in != null){
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				byte[] b = new byte[2048];
				int read=0;
				try {
					while ((read = in.read(b))!=-1){
						baos.write(b, 0, read);
					}
					baos.flush();
					return baos.toByteArray();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					
					return null;
				}	
			} else {
				return null;
			}
		} catch (FileNotFoundException e1) {
			logger.info("Swagger Override file - swagger.json for "+ environment +"/"+ appcodeName+" not present, interpretting from AppCode");
		}
		return null;
	}

}
