package com.dbsync.api.container;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Element;
import org.jdom.Namespace;

import com.appmashups.appcode.annotations.AppCodeFunction;
import com.appmashups.appcode.annotations.AppCodeParameter;

public class EDMBuilder {

	static Namespace edmx = Namespace.getNamespace("edmx", "http://docs.oasis-open.org/odata/ns/edmx");
	static Namespace edm = Namespace.getNamespace("http://docs.oasis-open.org/odata/ns/edm");
	
	Map<String, Class> entityTypeMap = new HashMap<String, Class>();
	
	public Element toEDM(Class cls){
		
		
		Element root = new Element("Edmx", edmx);
		Element dataService = new Element("DataServices",edmx);
		Element schema = new Element("Schema", edm)
						.setAttribute("Namespace", cls.getName());
		dataService.addContent(schema);
		
		Method [] allFunctions = cls.getMethods();
		
		List<Method> functionList = new ArrayList<Method>();
		
		Class[] exposedClasses = cls.getDeclaredClasses();
		for (Class<?> expcls: exposedClasses){
			System.out.println(expcls.getName());
			if (expcls.getAnnotation(AppCodeParameter.class)!=null){
				
				String key = expcls.getName();
				
				entityTypeMap.put(key, expcls);
			}
		}
		
		for (Method func: allFunctions){
			if (func.getAnnotation(AppCodeFunction.class)!=null){
				// look at in and out params and create the specs
				Class<?>[] argTypes = func.getParameterTypes();
				for (Class args: argTypes){

					if (!entityTypeMap.containsKey(args.getName())){
						entityTypeMap.put(args.getName(), args);
					}
				}
				Class<?> retType = func.getReturnType();
				Type retGenericType = func.getGenericReturnType();
				String typeName = retGenericType.getTypeName();
				
				if (retGenericType instanceof ParameterizedType){
					typeName = ((ParameterizedType)retGenericType).getActualTypeArguments()[0].getTypeName();
					retType = entityTypeMap.get(typeName);
					if (retType==null){
						try {
							retType = Class.forName(typeName);
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				
				if (!entityTypeMap.containsKey(typeName)){
					try {
						entityTypeMap.put(retType.getName(), Class.forName(typeName,false,retType.getClassLoader()));
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				functionList.add(func);
			}
		}
		
		for (Class entityClass: entityTypeMap.values()){
			Element entityType = toEntityType(entityClass);
			schema.addContent(entityType);
		}
		
		for (Method func: functionList){
			Element funcType = toFunction(func);
			schema.addContent(funcType);
		}
		
		root.addContent(dataService);
		
		root.setAttribute("Version", "1.0");
		return root;
	}
	
	private Element toEntityType(Class entityType) {
		Element type = new Element("EntityType", edm)
							.setAttribute("Name", entityType.getSimpleName());
		Field[] fields = entityType.getFields();
		
		for (Field fl: fields){
			Element prop = new Element("Property", edm)
								.setAttribute("Name", fl.getName())
								.setAttribute("Type", "Edm.String");
			type.addContent(prop);
			
		}
		
		return type;
	}

	/**
	 * <Function Name="GetFriendsTrips" IsBound="true" EntitySetPath="person/Friends/Trips" IsComposable="true">
		<Parameter Name="person" Type="Microsoft.OData.SampleService.Models.TripPin.Person" Nullable="false"/>
		<Parameter Name="userName" Type="Edm.String" Nullable="false"/>
		<ReturnType Type="Collection(Microsoft.OData.SampleService.Models.TripPin.Trip)" Nullable="false"/>
		</Function>
	 * @param method
	 * @return
	 */
	Element toFunction(Method method){
		Type retGenericType = method.getGenericReturnType();
		String typeName = retGenericType.getTypeName();
		boolean isCollection = typeName.startsWith("java.util.List");
		
		if (retGenericType instanceof ParameterizedType){
			typeName = ((ParameterizedType)retGenericType).getActualTypeArguments()[0].getTypeName();
		}
		
		Element function = new Element("Function", edm)
					.setAttribute("Name", method.getName());

		for (Class param: method.getParameterTypes()){
			Element paramElement = new Element("Parameter",edm)
								.setAttribute("Name", param.getSimpleName())
								.setAttribute("Type", param.getSimpleName());
			function.addContent(paramElement);
		}
		
		
		Class<?> retType = method.getReturnType();
		
		if (retGenericType instanceof ParameterizedType){
			typeName = ((ParameterizedType)retGenericType).getActualTypeArguments()[0].getTypeName();
			retType = entityTypeMap.get(typeName);
		}
		
		Element retElement=null;
		if (isCollection){
			retElement = new Element("CollectionType");
			
			Element typeElement = new Element("ReferenceType")
									.setAttribute("Type", retType.getSimpleName());
			retElement.addContent(typeElement);
			
		} else{
		
			retElement = new Element("ReturnType")
									.setAttribute("Name", retType.getSimpleName())
									.setAttribute("Type", retType.getName());
		}
		
		if (retElement!=null)
			function.addContent(retElement);

		return function;
	}
}
