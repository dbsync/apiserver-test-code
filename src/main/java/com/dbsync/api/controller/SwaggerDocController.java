package com.dbsync.api.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dbsync.api.container.AppCodeContainer;
import com.dbsync.api.container.AppCodeStorage;
import com.dbsync.api.container.SwaggerBuilder;

@RestController
public class SwaggerDocController {

	@RequestMapping(value = "/api-docs/{environment}/{appcodeName}/swagger", method = { RequestMethod.GET },
	// headers="Accept=application/json",
	produces = { MediaType.APPLICATION_JSON_VALUE })
	public String swagger(@PathVariable String environment,
			@PathVariable String appcodeName, final HttpServletRequest req,
			final HttpServletResponse resp) {

		String hostPort = req.getServerName() + ":" + req.getServerPort();
		String context = req.getContextPath();

		String key = environment + '/' + appcodeName;
		
		String runDir = req.getServletContext().getRealPath("/");
		
		AppCodeStorage container = AppCodeContainer.getStorage(environment, appcodeName,runDir);

		byte [] data = container.findResource(environment, appcodeName, "conf/swagger.json");
		
		// Yaml is not supported by the Javascript Swagger UI, so skipping it.
		// clients can use swagger editor to generate the code and create from yaml
		
		//if (data==null){
		//	data = container.findResource(environment, appcodeName, "resources/swagger.yaml");
		//}
		if (data != null){
			// we need to switch the host:port and context
			String swagger= new String(data)
							.replaceFirst("HOSTPORT", hostPort)
							.replaceFirst("CONTEXT", context)
							.replaceFirst("ENV", environment);
			return swagger;
		} else if (data == null){
			Object appCode = container.getAppCodeFromCache(appcodeName);
			if (appCode == null) {
				try {
					appCode = container.findAppCode(environment, appcodeName);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

					// TODO: Error handling
					return Error.error("Unable to find AppCode:"+environment+"/"+appcodeName, e);
				}
			}
			if (appCode != null) {
				// Open app code
				Class<?> appCodeClass = appCode.getClass();
				
				SwaggerBuilder builder = new SwaggerBuilder(hostPort, context,
						environment, appcodeName);

				String json = builder.toJSON(appCodeClass);

				return json;

			}
		}
		
		return Error.error("No docs found");

	}

}
