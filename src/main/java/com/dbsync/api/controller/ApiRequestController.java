package com.dbsync.api.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.appmashups.appcode.AppCodeConstants;
import com.appmashups.appcode.Context;
import com.dbsync.api.container.AppCodeContainer;
import com.dbsync.api.container.AppCodeStorage;
import com.dbsync.api.container.EDMBuilder;
import com.google.gson.Gson;

@RestController
public class ApiRequestController {
	static Logger logger = Logger.getLogger(ApiRequestController.class.getName());

	@RequestMapping(value = { "/appcode/{environment}/{appcodeName}/{functionName}/$metadata",
			"/apicode/{environment}/{appcodeName}/{functionName}/$metadata" }, method = {
					RequestMethod.GET }, produces = { MediaType.APPLICATION_XML_VALUE })
	public String metadata(@PathVariable String environment, @PathVariable String appcodeName,
			@PathVariable String functionName, final HttpServletRequest req, final HttpServletResponse resp) {

		String key = environment + '/' + appcodeName;

		logger.info("Call Metadata for :" + key);

		String runDir = req.getServletContext().getRealPath("/");

		AppCodeStorage container = AppCodeContainer.getStorage(environment, appcodeName, runDir);

		Object appCode = container.getAppCodeFromCache(appcodeName);
		if (appCode == null) {
			try {
				appCode = container.findAppCode(environment, appcodeName);
			} catch (Exception e) {
				// TODO: Error handling
				Error.error("AppCode does not exist", e);
			}
		}
		if (appCode != null) {
			// Open app code
			Class<?> appCodeClass = appCode.getClass();

			Document doc = new Document();
			Element edm = new EDMBuilder().toEDM(appCodeClass);
			doc.addContent(edm);

			XMLOutputter out = new XMLOutputter(Format.getPrettyFormat());

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			try {
				out.output(edm, bos);
				String xml = bos.toString();
				return xml;
			} catch (IOException e) {
				e.printStackTrace();
				Error.error("AppCode Error", e);
			}
		}
		return Error.error("Empty value");

	}

	@RequestMapping(value = { "/appcode/{environment}/{appcodeName}/{functionName}/**",
							"/apicode/{environment}/{appcodeName}/{functionName}/**" }, 
					method = { RequestMethod.GET }, 
					produces = { MediaType.APPLICATION_JSON_VALUE })
	public String processAPIGetRequest(@PathVariable String environment, @PathVariable String appcodeName,
			@PathVariable String functionName, @RequestParam Map<String, String> incomingParameters,
			final HttpServletRequest req, final HttpServletResponse resp) {
		return processAPIRequest(environment, appcodeName, functionName, new Gson().toJson(incomingParameters), req,
				resp);
	}

	@RequestMapping(value = { "/appcode/{environment}/{appcodeName}/{functionName}/**",
							"/apicode/{environment}/{appcodeName}/{functionName}/**" }, 
					method = { RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE }, 
					headers = "Accept=application/json", 
					produces = { MediaType.APPLICATION_JSON_VALUE })
	public String processAPIRequest(@PathVariable String environment, @PathVariable String appcodeName,
			@PathVariable String functionName, @RequestBody(required = false) String incomingParameter,
			final HttpServletRequest req, final HttpServletResponse resp) {

		String runDir = req.getServletContext().getRealPath("/");
		logger.info("Servlet Path:" + runDir);

		Object result = null;
		final String incomingParameterValue = incomingParameter;

		AppCodeStorage container = AppCodeContainer.getStorage(environment, appcodeName, runDir);

		Object appCode = container.getAppCodeFromCache(appcodeName);
		if (appCode == null) {
			try {
				appCode = container.findAppCode(environment, appcodeName);
			} catch (Exception e) {
				// TODO: Error handling
				return Error.error("AppCode does not exist", e);
			}
		}
		if (appCode == null) {
			return Error.error("AppCode:" + appcodeName + " does not exist, Please check your URL");
		}

		// Open app code
		Class<?> appCodeClass = appCode.getClass();
		try {
			Properties properties = new Properties();
			properties.put("server.addr", req.getServerName());
			properties.put("server.port", req.getServerPort());

			properties.put("application.context.path", req.getContextPath());
			Context context = new Context(properties);
			appCodeClass.getMethod("setContext", Context.class).invoke(appCode, context);
			appCodeClass.getMethod("open").invoke(appCode);
			// get env and app code name from request
			Method function = container.findFunction(appCode, functionName);
			if (function == null) {
				return Error.error("AppCode Method:" + functionName + " not found");
			}

			Class<?> incomingParameterClass = container.findAppCodeParameter(function, AppCodeConstants.IN);

			Gson gson = new Gson();
			try {
				Object incomingParamInstance = null;

				if (incomingParameterClass != null) {
					incomingParamInstance = gson.fromJson(incomingParameterValue, incomingParameterClass);
					result = function.invoke(appCode, incomingParamInstance);
				} else {
					result = function.invoke(appCode);
				}
				if ((result instanceof List)) {
					result = new ListRet((List<?>) result);
				}

			} catch (Exception e) {
				return Error.error("AppCode Method: " + function.getName() + " execution failed!", e);
			}
			return result == null ? Error.error("Empty value") : gson.toJson(result);
		} catch (Exception e) {
			return Error.error(e);
		}
	}

	class ListRet {
		List<?> value = null;

		public ListRet(List<?> val) {
			value = val;
		}
	}

}
