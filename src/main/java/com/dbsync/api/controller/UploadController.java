package com.dbsync.api.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.dbsync.api.container.AppCodeContainer;
import com.dbsync.api.container.AppCodeStorage;
@RestController
public class UploadController {
	
	protected static final Logger logger = Logger.getLogger(UploadController.class.getName());
	
	private final static String appCodeStoragePath = "/conf/db/%s/_appcode/";
	
	String profileName = "";
	String userName = "";
	
	//private final static String username = "localhost@avankia.com";
	private final static String environment = "sandbox";
	
	@Autowired
	private AppCodeStorage appcodeContainer;

	@RequestMapping(value = {"/uploadappcode", "/uploadappcode.m"},  method = { RequestMethod.POST, RequestMethod.GET }, produces = {MediaType.APPLICATION_JSON_VALUE})
	public String uploadAppCode(HttpSession session, HttpServletRequest request, HttpServletRequest response,
			@RequestParam("file") MultipartFile file) {
		final String userName = (String) request.getParameter("username");
		final String environment = (String) request.getParameter("environment");
		
		final String relativePathToAppCode = String.format(appCodeStoragePath, userName);
		final String storageRootPath = new File(getClass().getResource("/").getFile()).getParent();
		final File appCodeStorage = new File(storageRootPath + relativePathToAppCode + environment);
		if (!appCodeStorage.exists()) {
			appCodeStorage.mkdirs();
		}
		
		String appCodeFolderName = file.getOriginalFilename().replaceAll("\\.zip", "");
		final File appCodeDeployFolder = new File(storageRootPath + relativePathToAppCode + environment + "/" + appCodeFolderName);
		
		if (!appCodeDeployFolder.exists()) {
			appCodeDeployFolder.mkdirs();
		}
		
		// remove from storage, next time a call comes to it , getStorage will reconstruct it.
		// Late loading because we need to not have unused cache around.
		AppCodeContainer.removeStorage(environment,appCodeFolderName);
		
		if(upload(file, appCodeDeployFolder)){
			logger.log(Level.INFO, "AppCode archive " + file.getOriginalFilename() + " successfully uploaded!");
			return "{\"result\" : \"success\"}";
		}
		return "{\"result\" : \"error\"}";
	}

	private boolean upload(MultipartFile file, File userDir) {
		try {
			byte[] bytes = file.getBytes();
			if (file.getContentType() != null
					&& (file.getContentType().equalsIgnoreCase("application/x-zip-compressed")
							|| file.getContentType().equalsIgnoreCase("application/octet-stream")
							|| file.getContentType().equalsIgnoreCase("application/zip"))) {
				File serverFile = new File(userDir.getAbsolutePath() + "/" + file.getOriginalFilename());
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();
				unzip(serverFile, userDir.toString());
				serverFile.delete();
			} else {
				return false;
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	
	private int BUFFER_SIZE = 4096;
	private void unzip(File sourceFile, String destinationPath) throws IOException {
		ZipInputStream zipIn = new ZipInputStream(new FileInputStream(sourceFile));
		ZipEntry entry = zipIn.getNextEntry();
		while (entry != null) {
			String filePath = destinationPath + File.separator + entry.getName();
			if (!entry.isDirectory()) {
					extractFile(zipIn, filePath, "");
			} else {
				File dir = new File(filePath);
				dir.mkdir();
			}
			zipIn.closeEntry();
			entry = zipIn.getNextEntry();
		}
		zipIn.close();
	}

	private void extractFile(ZipInputStream zipIn, String filePath, String entityName) throws IOException {
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
		byte[] bytesIn = new byte[BUFFER_SIZE];
		int read = 0;
		while ((read = zipIn.read(bytesIn)) != -1) {
			bos.write(bytesIn, 0, read);
		}
		bos.close();
	}
}
