package com.dbsync.api.controller;

import com.google.gson.Gson;

public class Error {
	String error;
	String code;
	String detail;
	public Error(String e, String c, String d){
		this.error =e;
		this.code = c;
		this.detail = d;
	}
	
	protected static String error(Exception e) {
		Gson gson = new Gson();
		return gson.toJson(new Error(e.getMessage(),null, e.getStackTrace().toString()));
	}
	protected static String error(String s,Exception e) {
		Gson gson = new Gson();
		return gson.toJson(new Error(s,null, e.getMessage()));
	}
	protected static String error(String e) {
		Gson gson = new Gson();
		return gson.toJson(new Error(e,null, null));
	}
}