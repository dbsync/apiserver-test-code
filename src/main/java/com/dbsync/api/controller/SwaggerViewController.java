package com.dbsync.api.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SwaggerViewController {
	@RequestMapping(value = "/api-docs/{environment}/{appcodeName}", method = { RequestMethod.GET })
	public String docRequest(@PathVariable String environment,
			@PathVariable String appcodeName,
			final HttpServletRequest req, final HttpServletResponse resp, Model model) {
		
		//String hostPort = req.getServerName()+":"+req.getServerPort();
		String context =req.getContextPath();
		String url = context+"/api-docs/"+environment +"/"+ appcodeName+"/swagger";
		model.addAttribute("swaggerurl", url);
		
		return "swagger";
	}
}
