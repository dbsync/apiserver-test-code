package com.dbsync.api.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dbsync.api.container.AppCodeContainer;
import com.dbsync.api.container.AppCodeStorage;

@Controller
public class ApiListController {
static Logger logger = Logger.getLogger(ApiRequestController.class.getName());
	
	
	@RequestMapping(value = "/apilist", 
						method = {RequestMethod.GET} , 
						//headers="Accept=application/json", 
						produces = {MediaType.APPLICATION_XML_VALUE})
    public String list(final HttpServletRequest req, 
    								final HttpServletResponse resp,Model model) {
	
		String runDir = req.getServletContext().getRealPath("/");
		
		Map<String,AppCodeStorage> containerMap = AppCodeContainer.getAllStorage(runDir);
		
		Map<String, List<String>> apiVersionMap = new HashMap<String, List<String>>();
		for (String api : containerMap.keySet()) {
			String apiName = api.split("/")[1];
			if (apiVersionMap.containsKey(apiName)) {
				apiVersionMap.get(apiName).add(api.split("/")[0]);
			} else {
				List<String> vList = new ArrayList<String>();
				vList.add(api.split("/")[0]);
				apiVersionMap.put(apiName, vList);
			}
		}
		
		
		model.addAttribute("apiMap", apiVersionMap);
		
		return "apilist";
	}
}
