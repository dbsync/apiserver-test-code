package com.dbsync.monitor;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.openmbean.CompositeData;

import org.apache.zookeeper.AsyncCallback.StatCallback;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.KeeperException.Code;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

import com.google.gson.Gson;

public class RuntimeMonitor implements Watcher, StatCallback, Runnable {
	static Logger logger = Logger.getLogger(RuntimeMonitor.class.getName());
	
	ZooKeeper zk;

	String znode;
	String serverId;
	String serverNode;
	
	boolean dead=false;
	
	byte prevData[];


	Gson gson = new Gson();
	
	public class Measure{
		double cpu=0;
		long memory=0;
		long time = System.currentTimeMillis();
		
		public Measure(){
			
		}
		public void read(){
			try {
				time = System.currentTimeMillis();
				cpu = Server.cpu();
			    memory = Server.memory();
			    
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				logger.finer(e.getMessage());
			}
			
		}
		
		public byte[] toJSON(){
			String data = gson.toJson(this);
			logger.finer(data);
			return data.getBytes();
		}
	}
	
	Measure m = new Measure();
	
	public RuntimeMonitor(ZooKeeper zk, String appId) throws KeeperException, InterruptedException {
		this.zk = zk;
		this.znode = appId+"/runtime";;
		// we will use the last IP address as the identifier
	    // to uniquely identify, we will the process id
	    serverId = Server.id();//ManagementFactory.getRuntimeMXBean().getName().split("@")[0] + "@"+ip ;
	    serverNode = znode +"/"+serverId;
	  
	    
	    if (zk.exists(znode, false)==null){
	    	zk.create(znode, new byte[0], Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
	    }
	    
	    
		try {
			
			m.read();
			
			zk.create(serverNode, m.toJSON(), Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL);
			
			//zk.exists("/cw-server/"+serverId+"/cpu", true, this, null);
			
		} catch (KeeperException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			logger.log(Level.SEVERE,"ZooKeeper create error:",e);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			logger.log(Level.SEVERE,"ZooKeeper create error:",e);
		}
	}
	int count=1;
	@Override
	public void run() {
		while (!dead){
			try {
				m.read();
				
				zk.setData(serverNode, m.toJSON(), -1);
				
				Thread.currentThread().sleep(10000);
			} catch (KeeperException | InterruptedException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				logger.log(Level.SEVERE,"ZooKeeper send message error:",e);
			}
			
		}
	
	}
	

	public void process(WatchedEvent event) {
		String path = event.getPath();
		if (event.getType() == Event.EventType.None) {
			// We are are being told that the state of the
			// connection has changed
			switch (event.getState()) {
			case SyncConnected:
				// In this particular example we don't need to do anything
				// here - watches are automatically re-registered with
				// server and any watches triggered while the client was
				// disconnected will be delivered (in order of course)
				break;
			case Expired:
				// It's all over
				dead = true;
				break;
			}
		} else {
			if (path != null && path.equals(znode)) {
				// Something has changed on the node, let's find out
				zk.exists(znode, true, this, null);
			}
		}
	}

	public void processResult(int rc, String path, Object ctx, Stat stat) {
		boolean exists;
		switch (rc) {
		case Code.Ok:
			exists = true;
			break;
		case Code.NoNode:
			exists = false;
			break;
		case Code.SessionExpired:
		case Code.NoAuth:
			dead = true;
			return;
		default:
			// Retry errors
			zk.exists(znode, true, this, null);
			return;
		}

		byte b[] = null;
		if (exists) {
			try {
				b = zk.getData(znode, false, null);
			} catch (KeeperException e) {
				// We don't need to worry about recovering now. The watch
				// callbacks will kick off any exception handling
				e.printStackTrace();
			} catch (InterruptedException e) {
				return;
			}
		}
		//if ((b == null && b != prevData)
		//		|| (b != null && !Arrays.equals(prevData, b))) {
			//listener.exists(b);
		//	prevData = b;
		//}
	}


	static class Server {
		
		public static String ip(){
			String ip=null;
		    try {
		        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
		        while (interfaces.hasMoreElements()) {
		            NetworkInterface iface = interfaces.nextElement();
		            // filters out 127.0.0.1 and inactive interfaces
		            if (iface.isLoopback() || !iface.isUp())
		                continue;

		            Enumeration<InetAddress> addresses = iface.getInetAddresses();
		            while(addresses.hasMoreElements()) {
		                InetAddress addr = addresses.nextElement();
		                ip = addr.getHostAddress();
		                logger.finer(iface.getDisplayName() + " " + ip);
		            }
		        }
		    } catch (SocketException e) {
		        throw new RuntimeException(e);
		    }
		    return ip;
		}
		
		public static String id(){
			return pid() +"@"+ ip();
		}
		
		public static long pid(){
			return Long.parseLong(ManagementFactory.getRuntimeMXBean().getName().split("@")[0]);
		}
		
		public static double cpu(){
			try {
				MBeanServer mbs    = ManagementFactory.getPlatformMBeanServer();
				ObjectName name    = ObjectName.getInstance("java.lang:type=OperatingSystem");
				AttributeList list = mbs.getAttributes(name, new String[]{ "ProcessCpuLoad" });
				
				if (list.isEmpty())     
					return Double.NaN;
				
				Attribute att = (Attribute)list.get(0);
				Double value  = (Double)att.getValue();
				
				// usually takes a couple of seconds before we get real values
				if (value == -1.0)      
					return Double.NaN;
				// returns a percentage value with 1 decimal point precision
				return ((int)(value * 1000) / 10.0);
				
			} catch (InstanceNotFoundException | ReflectionException | MalformedObjectNameException | NullPointerException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				logger.log(Level.SEVERE,"ZooKeeper Runtime Monitor CPU check:",e);
			}
			
			return -1;
			
		}
		
		public static long memory(){
			MBeanServer mbs    = ManagementFactory.getPlatformMBeanServer();
		    Object o;
			try {
				o = mbs.getAttribute(new ObjectName("java.lang:type=Memory"), "HeapMemoryUsage");
				CompositeData cd = (CompositeData) o;
			    
			    return ((Long)cd.get("committed")).longValue();
			    
			} catch (AttributeNotFoundException | InstanceNotFoundException
					| MalformedObjectNameException | MBeanException
					| ReflectionException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				logger.log(Level.SEVERE,"ZooKeeper Runtime Monitor Memory check:",e);
			}
			
			return -1;
		    
		}
	}
	
}
