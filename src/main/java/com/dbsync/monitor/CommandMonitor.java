package com.dbsync.monitor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.zookeeper.ZooKeeper;

import com.google.gson.Gson;


public class CommandMonitor implements DataMonitor.DataMonitorListener {
	
	static Logger logger = Logger.getLogger(CommandMonitor.class.getName());
	ZooKeeper zk;
	
	class Command {
		String action;
		String value;
	}
	
	public CommandMonitor(ZooKeeper zk){
		this.zk = zk;
	}
	@Override
	public void exists(byte[] data) {
		if (data!=null){
			Gson gson = new Gson();
			
			Command cmd = gson.fromJson(new String(data), Command.class);
			
			try {
				Method method = this.getClass().getMethod(cmd.action, String.class);
				
				Object retObj = method.invoke(this, new Object[]{cmd.value});
				
				
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				logger.log(Level.SEVERE, "Cluster Command not found:"+cmd.action, e);
				// nothing to process
			}
			
		}
		
	}

	@Override
	public void closing(int rc) {
		// TODO Auto-generated method stub
		
	}

}
