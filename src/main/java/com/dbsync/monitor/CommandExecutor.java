package com.dbsync.monitor;

import java.io.IOException;
import java.util.logging.Logger;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

public class CommandExecutor implements Watcher, Runnable,
		DataMonitor.DataMonitorListener {
	
	static Logger logger = Logger.getLogger(CommandExecutor.class.getName());
	
	String znodeCommand;
	//String znodeRuntime;
	
	String appId;
	
	DataMonitor dm;
	RuntimeMonitor rm;

	ZooKeeper zk;


	public CommandExecutor(String hostPort, String app) throws KeeperException, IOException,InterruptedException {

		this.appId = app;
		
		if (!app.startsWith("/")){
			this.appId="/"+this.appId;
		}
		this.appId = this.appId +"-app";
		
		zk = new ZooKeeper(hostPort, 3000, this);
		
		// check to see if zNode exists, else create it
		Stat stat = zk.exists(appId, false);
		if (stat==null){
			zk.create(appId, new byte[0], Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
		}
		
		this.znodeCommand = this.appId+"/command";
		
		
		// check to see if zNode exists, else create it
		stat = zk.exists(znodeCommand, false);
		if (stat==null){
			zk.create(znodeCommand, new byte[0], Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
		}
		
		//dm = new DataMonitor(zk, znode, null, this);
		dm = new DataMonitor(zk, znodeCommand, null, new CommandMonitor(zk){
			
			public boolean kill(String value){
				// TODO:
				return true;
			}
		});
		
		
		// start the Server monitoring
		rm = new RuntimeMonitor(zk,this.appId);
		Thread t = new Thread(rm);
		t.start();
	}

	/***************************************************************************
	 * We do process any events ourselves, we just need to forward them on.
	 *
	 * @see org.apache.zookeeper.Watcher#process(org.apache.zookeeper.proto.WatcherEvent)
	 */
	public void process(WatchedEvent event) {
		dm.process(event);
		//if (event.getPath()!=null && event.getPath().startsWith(this.znodeRuntime)){
			
		//}
	}

	public void run() {
		/*
		try {
			synchronized (this) {
				while (!dm.dead) {
					wait();
				}
			}
		} catch (InterruptedException e) {
		}*/
	}

	public void closing(int rc) {
		synchronized (this) {
			notifyAll();
		}
	}


	public void exists(byte[] data) {
		if (data == null) {
			//if (child != null) {
				//System.out.println("Killing process");
				/*child.destroy();
				try {
					child.waitFor();
				} catch (InterruptedException e) {
				}*/
			//}
			//child = null;
		} else {
			//if (child != null) {
				//System.out.println("Got:"+ new String(data));
				//child.destroy();
				//try {
				//	child.waitFor();
				//} catch (InterruptedException e) {
				//	e.printStackTrace();
				//}
			//}
				/*
			try {
				FileOutputStream fos = new FileOutputStream(filename);
				fos.write(data);
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				System.out.println("Starting child");
				//child = Runtime.getRuntime().exec(exec);
				//new StreamWriter(child.getInputStream(), System.out);
				//new StreamWriter(child.getErrorStream(), System.err);
			} catch (Exception e) {
				e.printStackTrace();
			}*/
		}
	}
}
