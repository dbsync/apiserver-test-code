package com.dbsync.monitor;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.InitializingBean;


public class ZooKeeperBean implements InitializingBean{
	static Logger logger = Logger.getLogger(ZooKeeperBean.class.getName());

	CommandExecutor executor=null;
	
	String hostPort = "";
	boolean enabled = false;
	
	String appId="NOTSET";
	
	public ZooKeeperBean(){}
	
	public void setHostport(String hostPort) {
		this.hostPort = hostPort;
		if (this.hostPort==null || this.hostPort.length()==0){
			enabled=false;
		} else {
			enabled = true;
		}
	}
	
	public void setAppId(String appId) {
		this.appId = appId;
	}

	
	@PostConstruct
	public void init(){
		if (this.enabled){
			boolean connected =false;
			
			//while (!connected){
				try {
					executor = new CommandExecutor(hostPort, this.appId);
					executor.run();
					connected=true;
				} catch (Exception e) {
					//e.printStackTrace();
					logger.severe("Unable to connect to ZooKeeper:"+ hostPort);
					
					new ZooKeeperConnectThread().start();
				}
			//}
		}
		
	}
	
	public void close(){
		try {
			executor.zk.close();
		} catch (Exception e) {}
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	class ZooKeeperConnectThread extends Thread{

		@Override
		public void run() {
			boolean connected =false;
			while (!connected){
				try {
					executor = new CommandExecutor(hostPort, appId);
					executor.run();
					
					logger.info("Connected to ZooKeeper:"+ hostPort);
					
					connected=true;
				} catch (Exception e) {

					logger.severe("Unable to connect to ZooKeeper:"+ hostPort+"-"+e.getMessage());
					
					try {
						Thread.currentThread().sleep(10000);
					} catch (InterruptedException e1) {}
				}
				
			}
			
		}
		
	}
}
