<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Documentation</title>
  <link rel="icon" type="image/png" href='<c:url value="/docs/images/favicon-32x32.png"/>' sizes="32x32" />
  <link rel="icon" type="image/png" href='<c:url value="/docs/images/favicon-16x16.png"/>' sizes="16x16" />
  <link href='<c:url value="/docs/css/typography.css"/>' media='screen' rel='stylesheet' type='text/css'/>
  <link href='<c:url value="/docs/css/reset.css"/>' media='screen' rel='stylesheet' type='text/css'/>
  <link href='<c:url value="/docs/css/screen.css"/>' media='screen' rel='stylesheet' type='text/css'/>
  <link href='<c:url value="/docs/css/reset.css"/>' media='print' rel='stylesheet' type='text/css'/>
  <link href='<c:url value="/docs/css/print.css"/>' media='print' rel='stylesheet' type='text/css'/>
  <script src='<c:url value="/docs/lib/jquery-1.8.0.min.js"/>' type='text/javascript'></script>
  <script src='<c:url value="/docs/lib/jquery.slideto.min.js"/>' type='text/javascript'></script>
  <script src='<c:url value="/docs/lib/jquery.wiggle.min.js"/>' type='text/javascript'></script>
  <script src='<c:url value="/docs/lib/jquery.ba-bbq.min.js"/>' type='text/javascript'></script>
  <script src='<c:url value="/docs/lib/handlebars-2.0.0.js"/>' type='text/javascript'></script>
  <script src='<c:url value="/docs/lib/underscore-min.js"/>' type='text/javascript'></script>
  <script src='<c:url value="/docs/lib/backbone-min.js"/>' type='text/javascript'></script>
  <script src='<c:url value="/docs/swagger-ui.min.js"/>' type='text/javascript'></script>
  <script src='<c:url value="/docs/lib/highlight.7.3.pack.js"/>' type='text/javascript'></script>
  <script src='<c:url value="/docs/lib/jsoneditor.min.js"/>' type='text/javascript'></script>
  <script src='<c:url value="/docs/lib/marked.js"/>' type='text/javascript'></script>
  <script src='<c:url value="/docs/lib/swagger-oauth.js"/>' type='text/javascript'></script>

  <!-- Some basic translations -->
  <!-- <script src='lang/translator.js' type='text/javascript'></script> -->
  <!-- <script src='lang/ru.js' type='text/javascript'></script> -->
  <!-- <script src='lang/en.js' type='text/javascript'></script> -->

  <script type="text/javascript">
    $(function () {
      var url = window.location.search.match(/url=([^&]+)/);
      if (url && url.length > 1) {
        url = decodeURIComponent(url[1]);
      } else {
        url = '${swaggerurl}';
        //url="api-with-examples.json";
        
      }

      // Pre load translate...
      if(window.SwaggerTranslator) {
        window.SwaggerTranslator.translate();
      }
      window.swaggerUi = new SwaggerUi({
        url: url,
        dom_id: "swagger-ui-container",
        supportedSubmitMethods: ['get', 'post', 'put', 'delete', 'patch'],
        onComplete: function(swaggerApi, swaggerUi){
          
          if(window.SwaggerTranslator) {
            window.SwaggerTranslator.translate();
          }

          $('pre code').each(function(i, e) {
            hljs.highlightBlock(e)
          });

        },
        onFailure: function(data) {
          log("Unable to Load SwaggerUI");
        },
        docExpansion: "none",
        jsonEditor: true,
        apisSorter: "alpha",
        defaultModelRendering: 'model',
        showRequestHeaders: false
      });



      window.swaggerUi.load();

      function log() {
        if ('console' in window) {
          console.log.apply(console, arguments);
        }
      }
  });
  </script>
</head>

<body class="swagger-section">
<div id='header'>
  <div class="swagger-ui-wrap">
    <a id="logo" href="#">DBSync AppCode</a>
    <form id='api_selector'>
      <div class='input'><input placeholder="http://example.com/api" id="input_baseUrl" name="baseUrl" type="text"/></div>
      <div class='input'><a id="explore" href="#" data-sw-translate>Explore</a></div>
    </form>
  </div>
</div>

<div id="message-bar" class="swagger-ui-wrap" data-sw-translate>&nbsp;</div>
<div id="swagger-ui-container" class="swagger-ui-wrap"></div>
</body>
</html>
