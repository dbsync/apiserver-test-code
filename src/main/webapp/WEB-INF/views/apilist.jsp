<%@ page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>

<head>

<style type="text/css">
table.dataTable thead .sorting, table.dataTable thead .sorting:after,
	table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc,
	table.dataTable thead .sorting_desc {
	background: none;
	content: none !important;
}

body:after {
	content: "beta";
	position: fixed;
	width: 80px;
	height: 25px;
	background: #EE8E4A;
	top: 7px;
	right: -20px;
	text-align: center;
	font-size: 13px;
	font-family: sans-serif;
	text-transform: uppercase;
	font-weight: bold;
	color: #fff;
	line-height: 27px;
	-ms-transform: rotate(45deg);
	-webkit-transform: rotate(45deg);
	transform: rotate(45deg);
}

.icon_dbsync:before {
	content: "";
	display: block;
	background: url("<c:url value="/ scripts/ inspinia/ img/ dbsync_logo.jpg "/>")
		no-repeat;
	width: 40px;
	height: 40px;
	float: left;
	margin: 0px 7px 0 0;
}
</style>


<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="shortcut icon"
	href="<c:url value="/scripts/inspinia/img/favicon.ico" />" />

<link href="<c:url value="/scripts/inspinia/css/bootstrap.min.css" />"
	rel="stylesheet" />
<link
	href="<c:url value="/scripts/inspinia/font-awesome/css/font-awesome.css" />" />

<link href="<c:url value="/scripts/inspinia/css/animate.css" />"
	rel="stylesheet" />
<link href="<c:url value="/scripts/inspinia/css/style.css" />"
	rel="stylesheet" />

<link
	href="<c:url value="/scripts/inspinia/css/dataTables/dataTables.bootstrap.css"  />"
	rel="stylesheet" />
<link
	href="<c:url value="/scripts/inspinia/css/dataTables/dataTables.responsive.css"  />"
	rel="stylesheet" />
<link
	href="<c:url value="/scripts/inspinia/css/dataTables/dataTables.tableTools.min.css"  />"
	rel="stylesheet" />

<title>DBSync APIs</title>
</head>

<body class="gray-bg">

	<div id="wrapper">
		<div id="page-wrapper-1">
			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-lg-10">
					<h2>
						<span class='icon_dbsync'></span> DBSync Server API
					</h2>
				</div>
				<div class="col-lg-2"></div>
			</div>

			<div class="wrapper wrapper-content">

				<div class="row">
					<div class="col-lg-12">
						<div class="ibox">
							<div class="ibox-content">

								<table id="apiListTable"
									class="table table-stripped table-bordered table-hover toggle-arrow-tiny">
									<thead>
										<tr>

											<th data-toggle="false">API Name</th>
											<th>Version(s)</th>

										</tr>
									</thead>
									<tbody>

										<%
											Map<String, List<String>> apiMap = (Map<String, List<String>>) request.getAttribute("apiMap");
											for (String apiName : apiMap.keySet()) {
										%>

										<tr>
											<td><%=apiName%></td>
											<td>
												<%
													Collections.sort(apiMap.get(apiName));
														for (String version : apiMap.get(apiName)) {
															String versionLink = "/api-docs/" + version + "/" + apiName;
												%> <a href="<c:url value="<%=versionLink%>" />"
												target="_new"><%=version%></a> &nbsp;&nbsp; <%
 	}
 %>
											</td>
										</tr>
										<%
											}
										%>

									</tbody>

									<tr>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>


			</div>

		</div>
	</div>
	<div class="footer">
		<div>
			<strong>Copyright</strong> DBSync LLC. � 2009-2017 All rights
			reserved. DBSync is a trademark owned by DBSync LLC. All other
			trademarks are owned by its respective companies.
		</div>
	</div>



	<!-- Mainly scripts -->
	<script src="<c:url value="/scripts/inspinia/js/jquery-2.1.1.js" />"></script>
	<script src="<c:url value="/scripts/inspinia/js/bootstrap.min.js" />"></script>

	<script
		src="<c:url value="/scripts/inspinia/js/plugins/metisMenu/jquery.metisMenu.js" />"></script>
	<script
		src="<c:url value="/scripts/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js" />"></script>

	<!-- Custom and plugin javascript -->
	<script src="<c:url value="/scripts/inspinia/js/inspinia.js" />"></script>

	<script
		src="<c:url value="/scripts/inspinia/js/plugins/dataTables/jquery.dataTables.js" />"></script>
	<script
		src="<c:url value="/scripts/inspinia/js/plugins/dataTables/dataTables.bootstrap.js" />"></script>
	<script
		src="<c:url value="/scripts/inspinia/js/plugins/dataTables/dataTables.responsive.js" />"></script>
	<script
		src="<c:url value="/scripts/inspinia/js/plugins/dataTables/dataTables.tableTools.min.js" />"></script>


	<!-- Page-Level Scripts -->
	<script>
		$(document).ready(function() {
			$('#apiListTable').dataTable({
				responsive : true,
				bPaginate : false,
				bInfo : false,
				language : {
					searchPlaceholder : "Search APIs"
				}
			});
		});
	</script>

</body>

</html>
